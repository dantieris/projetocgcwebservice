<?php

class TipoCombustivel {

	private $id;
	private $preco;
	private $tipo;
	private $combustivel;
	private $posto
	
	function __construct($id="", $preco="", $tipo="", $combustivel="", $posto="") {
		$this->id = $id;
		$this->preco = $preco;
		$this->tipo = $tipo;
		$this->combustivel = $combustivel;
		$this->posto = $posto;
	}

	function __set($prop, $val) {
		$this->$prop = $val;
	}

	function __get($prop) {
		return $this->$prop;
	}

}

?>