<?php 

include_once("BancoPDO.class.php");

class TipoCombustivelDAO extends BancoPDO {

  public function __construct() {
    $this->conexao = BancoPDO::conexao();
  }

  public function getTipoCombustivelPorPosto($idPosto) {
    try {

      $stm = $this->conexao->prepare("SELECT TIPOS_COMBUSTIVEL.ID, TIPOS.NOME AS TIPO, COMBUSTIVEIS.NOME AS COMBUSTIVEL, PRECO FROM `TIPOS_COMBUSTIVEL`
                                      INNER JOIN TIPOS
                                        ON TIPOS.ID = TIPOS_COMBUSTIVEL.ID_TIPO
                                      INNER JOIN COMBUSTIVEIS
                                        ON COMBUSTIVEIS.ID = TIPOS_COMBUSTIVEL.ID_COMBUSTIVEL
                                      WHERE TIPOS_COMBUSTIVEL.ID_POSTO = ?");

      $stm->bindValue(1, $idPosto);
      $stm->execute();

      $tipoCombustivel = $stm->fetchAll(PDO::FETCH_OBJ);

      echo "{\"tipoCombustivel\":".json_encode($tipoCombustivel)."}";
  
      } catch (PDOException $e) {
        echo "Erro: ".$e->getMessage();
      }
  }
  
  public function atualizarPreco() {
    $request = \Slim\Slim::getInstance()->request();
    $atualizaPreco = json_decode($request->getBody());

    $id = $atualizaPreco->ID;
    $preco = $atualizaPreco->PRECO;
    $id_android = $atualizaPreco->ID_ANDROID;

    $stm = $this->conexao->prepare("START TRANSACTION;
									INSERT INTO ALTERACOES_PRECOS_AUDITORIA (ID_ANDROID, DATA, PRECO_ANTIGO, PRECO_NOVO, ID_TIPOS_COMBUSTIVEL)
									VALUES (?, NOW(), (SELECT PRECO FROM TIPOS_COMBUSTIVEL WHERE ID = ?), ?, ?);
									COMMIT;");

    $stm->bindValue(1, $id_android);
    $stm->bindValue(2, $id);
    $stm->bindValue(3, $preco);
    $stm->bindValue(4, $id);

    $stm->execute();

    $stm = $this->conexao->prepare("START TRANSACTION;
									UPDATE `TIPOS_COMBUSTIVEL`
									SET `PRECO` = ? WHERE `ID` = ?;
									COMMIT;");
      
    $stm->bindValue(1, $preco);
    $stm->bindValue(2, $id);        

    $stm->execute();
        
    echo "{\"tipoCombustivel\":".json_encode($atualizaPreco)."}";
  }
  
}

?>	