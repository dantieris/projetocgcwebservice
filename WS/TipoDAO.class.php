<?php 

include_once("BancoPDO.class.php");

class TipoDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}

	public function getBackupTipos() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM TIPOS");
		
			$stm->execute();

			$tipo = $stm->fetchAll(PDO::FETCH_OBJ);
			
			echo "{\"BACKUP_TIPOS\":".json_encode($tipo)."}";
	
			} catch (PDOException $e) {
				echo "Erro: ".$e->getMessage();
			}
	}

}

?>