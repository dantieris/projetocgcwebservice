<?php 

include_once("BancoPDO.class.php");

class BandeiraDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}

	public function getBackupBandeiraS() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM BANDEIRAS");
			$stm->execute();

			$bandeira = $stm->fetchAll(PDO::FETCH_OBJ);

			echo "{\"BACKUP_BANDEIRAS\":".json_encode($bandeira)."}";
	
			} catch (PDOException $e) {
				echo "Erro: ".$e->getMessage();
			}
	}

}

?>