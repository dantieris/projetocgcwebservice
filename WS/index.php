<?php

require_once('../Slim/Slim/Slim.php');
require_once('PostoDAO.class.php');
require_once('BandeiraDAO.class.php');
require_once('TipoDAO.class.php');
require_once('CombustivelDAO.class.php');
require_once('TipoCombustivelDAO.class.php');
require_once('AbastecimentoDAO.class.php');
require_once('PriceDAO.class.php');

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/', function () {
  echo "Webservice Card Prices";
});

$app->get('/prices/:edition/:number', function ($edition, $number){
  if (!empty($edition) && !empty($number) {
    $posto = new PriceDAO();
    $result = $posto->getPrices($edition, $number);
  } else {
    $result = array("status" => "erro", "message" => "O nome da edição e o número da carta são obrigatórios.");
  }
  
  echo json_encode($result);
});

$app->get('/posto/:id/:chave', function ($id, $chave){
  if (verificarChave($chave)) {
    $posto = new PostoDAO();
    $posto->getPosto($id);
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

$app->get('/tipoCombustivel/:id/:chave', function ($id, $chave){
  if (verificarChave($chave)) {
    $tipoCombustivel = new TipoCombustivelDAO();
    $tipoCombustivel->getTipoCombustivel($id);
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

$app->get('/tipoCombustivelPorPosto/:id/:chave', function ($idPosto, $chave){
  if (verificarChave($chave)) {
    $tipoCombustivel = new TipoCombustivelDAO();
    $tipoCombustivel->getTipoCombustivelPorPosto($idPosto);
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

$app->post('/atualizarPreco/:chave', function ($chave){
  if (verificarChave($chave)) {
    $tipoCombustivel = new TipoCombustivelDAO();
    $tipoCombustivel->atualizarPreco();
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

$app->post('/inserirAbastecimento/:chave', function ($chave){
  if (verificarChave($chave)) {
    $abastecimento = new AbastecimentoDAO();
    $abastecimento->inserirAbastecimento();
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

$app->get('/relatorio/:id_android/:chave', function ($id_android, $chave){
  if (verificarChave($chave)) {
    $abastecimento = new AbastecimentoDAO();
    $abastecimento->getAbastecimentos($id_android);
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

// Serviços de backup

$app->get('/BACKUP_TIPOS/:chave', function ($chave){
  if (verificarChave($chave)) {
  $tipo = new TipoDAO();
  $tipo->getBackupTipos();
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

$app->get('/BACKUP_BANDEIRAS/:chave', function ($chave){
  if (verificarChave($chave)) {
  $bandeira = new BandeiraDAO();
  $bandeira->getBackupBandeiras();
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }

});

$app->get('/BACKUP_COMBUSTIVEIS/:chave', function ($chave){
  if (verificarChave($chave)) {  
    $combustivel = new CombustivelDAO();
    $combustivel->getBackupCombustiveis();
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }

});

$app->get('/BACKUP_POSTOS/:chave', function ($chave){
  if (verificarChave($chave)) {
    $posto = new PostoDAO();
    $posto->getBackupPostos();
  } else {
    $resultado = array("estado" => "erro", "mensagem" => "Você precisa de uma chave válida.");
    echo json_encode($resultado);
  }
});

function verificarChave($chave) {
  if ($chave === 'WSd55069b2050e1559c7846b75b6544104')
    return true;
}

$app->run();