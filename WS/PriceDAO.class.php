<?php 

include_once("BancoPDO.class.php");

class PriceDAO extends BancoPDO {

  public function __construct() {
    $this->conexao = BancoPDO::conexao();
  }

  public function getPrices($edition, $number) {
    try {

      $stm = $this->conexao->prepare("select low, mid, max from prices where edition = :edition and number = :number");

      $stm->bindValue(':edition', $edition);
      $stm->bindValue(':number', $number);
      $stm->execute();

      $prices = $stm->fetchAll(PDO::FETCH_OBJ);

      if (!empty($prices)) {
           $prices = [
               'prices' => $prices,
           ];
      }

      return $prices;
  
      } catch (PDOException $e) {
          return [
               'status' => 'error',
               'message' => $e->getMessage()
          ];
      }
  }
}

?>