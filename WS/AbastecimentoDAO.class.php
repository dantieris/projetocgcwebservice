<?php 

include_once("BancoPDO.class.php");

class AbastecimentoDAO extends BancoPDO {

  public function __construct() {
    $this->conexao = BancoPDO::conexao();
  }

  public function inserirAbastecimento() {
    $request = \Slim\Slim::getInstance()->request();
    $abastecimento = json_decode($request->getBody());

    try { 
      $stm = $this->conexao->prepare("START TRANSACTION;
									INSERT INTO `ABASTECIMENTOS` 
                                    (`ID_TIPOS_COMBUSTIVEL`, `VALOR_TOTAL`, `LITROS`, `ID_ANDROID`,`DATA` ) 
                                    VALUES (?, ?, ?, ?, ?);
									COMMIT;");

      $id_tipos_combustivel = $abastecimento->tipoCombustivel->id;
      $valor_total = $abastecimento->valor_total;
      $litros = $abastecimento->litros;
      $id_android = $abastecimento->id_android;
      $data = $abastecimento->data;

      $stm->bindValue(1, $id_tipos_combustivel);
      $stm->bindValue(2, $valor_total);
      $stm->bindValue(3, $litros);
      $stm->bindValue(4, $id_android);
      $stm->bindValue(5, $data);

      $stm->execute();
    } catch(PDOException $e) {
      echo "Erro: ".$e->getMessage();
    }
  }

  public function getAbastecimentos($id_android) {
    try {

      // Seleciona todos os abastecimentos do id android.
      $stm = $this->conexao->prepare("SELECT ABASTECIMENTOS.ID, COMBUSTIVEIS.NOME AS COMBUSTIVEL, TIPOS.NOME AS TIPO, VALOR_TOTAL, LITROS, ID_ANDROID, Day(DATA) AS 'DIA', Month(DATA) AS 'MES', Year(DATA) AS 'ANO', POSTOS.NOME AS POSTO
                                      FROM `ABASTECIMENTOS`
                                      INNER JOIN TIPOS_COMBUSTIVEL
                                      ON TIPOS_COMBUSTIVEL.ID = ABASTECIMENTOS.ID_TIPOS_COMBUSTIVEL
                                      INNER JOIN TIPOS
                                      ON TIPOS.ID = TIPOS_COMBUSTIVEL.ID_TIPO
                                      INNER JOIN COMBUSTIVEIS
                                      ON COMBUSTIVEIS.ID = TIPOS_COMBUSTIVEL.ID_COMBUSTIVEL
                                      INNER JOIN POSTOS
                                      ON POSTOS.ID = TIPOS_COMBUSTIVEL.ID_POSTO
                                      WHERE ID_ANDROID = ? AND (DATA BETWEEN  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() )
                                      ORDER BY DATA DESC, VALOR_TOTAL DESC");
      
      $stm->bindValue(1, $id_android);
      $stm->execute();

      $abastecimentos = $stm->fetchAll(PDO::FETCH_OBJ);

      // Seleciona qual combustivel foi mais utilizado
      $stm = $this->conexao->prepare("SELECT ID_TIPOS_COMBUSTIVEL, COUNT( ID_TIPOS_COMBUSTIVEL ) AS QUANTIDADE, CONCAT(COMBUSTIVEIS.NOME,' ',TIPOS.NOME) AS NOME
                                      FROM ABASTECIMENTOS
                                      INNER JOIN TIPOS_COMBUSTIVEL
                                        ON TIPOS_COMBUSTIVEL.ID = ABASTECIMENTOS.ID_TIPOS_COMBUSTIVEL
                                      INNER JOIN COMBUSTIVEIS
                                        ON TIPOS_COMBUSTIVEL.ID_COMBUSTIVEL = COMBUSTIVEIS.ID
                                      INNER JOIN TIPOS
                                        ON TIPOS.ID = TIPOS_COMBUSTIVEL.ID_TIPO
                                      WHERE ID_ANDROID =  ? AND (DATA BETWEEN  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() )
                                      GROUP BY ID_TIPOS_COMBUSTIVEL
                                      ORDER BY QUANTIDADE DESC 
                                      LIMIT 0 , 1");

      $stm->bindValue(1, $id_android);
      $stm->execute();

      $combustivelMaisUsado  = $stm->fetchAll(PDO::FETCH_OBJ);

      // Calcula o valor total abastecido
      $stm = $this->conexao->prepare("SELECT SUM(VALOR_TOTAL) AS VALOR_TOTAL_ABASTECIDO
                                      FROM ABASTECIMENTOS
                                      WHERE ID_ANDROID = ?  AND (DATA BETWEEN  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() )");

      $stm->bindValue(1, $id_android);
      $stm->execute();

      $valorTotalAbastecido  = $stm->fetchAll(PDO::FETCH_OBJ);

      // Seleciona qual posto foi mais utilizado
      $stm = $this->conexao->prepare("SELECT ID_POSTO, COUNT( ID_POSTO ) AS QUANTIDADE, POSTOS.NOME
                                    FROM ABASTECIMENTOS
                                    INNER JOIN TIPOS_COMBUSTIVEL 
                                      ON TIPOS_COMBUSTIVEL.ID = ABASTECIMENTOS.ID_TIPOS_COMBUSTIVEL
                                    INNER JOIN POSTOS
                                      ON TIPOS_COMBUSTIVEL.ID_POSTO = POSTOS.ID
                                    WHERE ID_ANDROID =  ? AND (DATA BETWEEN  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() )
                                    GROUP BY ID_POSTO
                                    ORDER BY QUANTIDADE DESC 
                                    LIMIT 0 , 1");

      $stm->bindValue(1, $id_android);
      $stm->execute();

      $postoMaisUsado  = $stm->fetchAll(PDO::FETCH_OBJ);

      $abastecimentos["valorTotalAbastecido"] = $valorTotalAbastecido[0];
      $abastecimentos["combustivelMaisUsado"] = $combustivelMaisUsado[0];
      $abastecimentos["postoMaisUsado"] = $postoMaisUsado[0];

      echo "{\"relatorio\":".json_encode($abastecimentos)."}";

  
      } catch (PDOException $e) {
        echo "Erro: ".$e->getMessage();
      }
  }
  
}

?>