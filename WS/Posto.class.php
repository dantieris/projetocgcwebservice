<?php

class Posto {

	private $id;
	private $nome;
	private $latitude;
	private $longitude;
	private $endereco;
	private $bandeira;
	
	function __construct($id="", $endereco="", $nome="", $latitude=0.0, $longitude=0.0, $bandeira="") {
		
		$this->id = $id;
		$this->endereco = $endereco;
		$this->nome = $nome;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
		$this->bandeira = $bandeira;
	}

	function __set($prop, $val) {
		$this->$prop = $val;
	}

	function __get($prop) {
		return $this->$prop;
	}

}

?>