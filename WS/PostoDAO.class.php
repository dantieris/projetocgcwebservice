<?php 

include_once("BancoPDO.class.php");

class PostoDAO extends BancoPDO {

  public function __construct() {
    $this->conexao = BancoPDO::conexao();
  }

  public function getPostos() {
    try {

      $stm = $this->conexao->prepare("SELECT POSTOS.*,BANDEIRAS.NOME AS BANDEIRA FROM POSTOS
                                      INNER JOIN BANDEIRAS
                                        ON BANDEIRAS.ID = POSTOS.ID_BANDEIRA");
      $stm->execute();

      $postos = $stm->fetchAll(PDO::FETCH_OBJ);

      echo "{\"postos\":".json_encode($postos)."}";
  
      } catch (PDOException $e) {
        echo "Erro: ".$e->getMessage();
      }
  }

  public function getPosto($id) {
    try {

      $stm = $this->conexao->prepare("SELECT POSTOS.*,BANDEIRAS.NOME AS BANDEIRA FROM POSTOS
                                      INNER JOIN BANDEIRAS
                                        ON BANDEIRAS.ID = POSTOS.ID_BANDEIRA
                                      WHERE POSTOS.ID = ?");

      $stm->bindValue(1, $id);
      $stm->execute();

      $posto = $stm->fetchAll(PDO::FETCH_OBJ);

      echo "{\"posto\":".json_encode($posto)."}";
  
      } catch (PDOException $e) {
        echo "Erro: ".$e->getMessage();
      }
  }
  
  public function getBackupPostos() {
    try {

      $stm = $this->conexao->prepare("SELECT * FROM POSTOS");
      $stm->execute();

      $postos = $stm->fetchAll(PDO::FETCH_OBJ);

      echo "{\"BACKUP_POSTOS\":".json_encode($postos)."}";
  
      } catch (PDOException $e) {
        echo "Erro: ".$e->getMessage();
      }
  }
}

?>