<?php 

include_once("BancoPDO.class.php");

class CombustivelDAO extends BancoPDO {

	public function __construct() {
		$this->conexao = BancoPDO::conexao();
	}

	public function getBackupCombustiveis() {
		try {

			$stm = $this->conexao->prepare("SELECT * FROM COMBUSTIVEIS");
			$stm->execute();

			$combustivel = $stm->fetchAll(PDO::FETCH_OBJ);

			echo "{\"BACKUP_COMBUSTIVEIS\":".json_encode($combustivel)."}";
	
			} catch (PDOException $e) {
				echo "Erro: ".$e->getMessage();
			}
	}



}

?>